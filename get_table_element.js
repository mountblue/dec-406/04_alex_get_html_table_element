'use strict'

/**
 * name: get_table_element.js
 * description: "A simple script to extract table contents and output as table on console"
 * input: url (with well formed html5 table element)
 * output: table on console
 * usage: node get_table_element.js --url|-u <url>
 * example: node get_table_element.js -u 'https://en.wikipedia.org/wiki/Programming_languages_used_in_most_popular_websites'
 */

const rp = require('request-promise'),
      cheerio = require('cheerio'),
      cli = require('command-line-args'),
      clu = require('command-line-usage'),
      table = require('table');

/**
 * define command-line-usage guides and check usage
 * documentation @ https://www.npmjs.com/package/command-line-usage
 */
const optionsDefs = [
    {"name": "url", "alias": "u", "type": String},
];

let options = {};

/**
* define command-line-usage guides and check usage
* documentation @ https://www.npmjs.com/package/command-line-usage
*/
const sections = [
    {
      "header": "html <table> parser",
      "content": "A simple script to extract table contents and output as table on console"
    },
    {
      "header": "Options",
      "optionList": [
          {
              "name": "url",
              "alias": "u",
              "typeLabel": "{underline url link}",
              "description": "The website whose table to process"
          }
      ]
    }
];

// check for bad user input
try {
    options = cli(optionsDefs);
} catch (err) {
    if (err !== null) {
        console.error('Invalid Usage');
        console.error(err.message);
    }
} finally {
    if (!('url' in options)) {
        console.log(clu(sections));
        process.exit();
    }
}

/**
 * request-promise to read a web DOM tree
 * cheerio to traverse DOM tree
 * @cheerio doc https://www.npmjs.com/package/cheerio
 * @request-promise https://www.npmjs.com/package/request-promise
 * @output {String} table~cell @table
 */

try {
    rp(options.url)
        .then(html => {
            let htmlTable = cheerio("table", html);
            // final JSON object
            let tableJson = {};
            htmlTable.each((index, tableElem) => {
                let tableName = cheerio('caption', tableElem).text().trim();
                let tableHead = cheerio('th', tableElem).text().trim().split('\n');
                tableJson[tableName] = [];
                cheerio('tbody', tableElem).each((index, row) => {
                    cheerio('tr', row).each((index, data) => {
                        let jsonObj = {};
                        // read text value from 'td' element
                        let rowData = cheerio('td', data).text().trim().split('\n');
                        if (rowData[0]) {
                            for (let i = 0, n = tableHead.length; i < n; i++) {
                                jsonObj[tableHead[i]] = rowData[i] ? rowData[i] : 'none';
                            }
                            tableJson[tableName].push(jsonObj);
                        }
                    });
                });
            });

            for (let key in tableJson) {
                console.log(table.table([[key]], {0: {alignment: 'left'}}));
                let props = Object.keys(tableJson[key][0]);
                let data = [props];
                let config = tableConfig(props.length, props);
                // console.log(config);
                data = [...data, ...tableData(tableJson[key])];
                // console.log(data);
                // final output
                console.log(table.table(data, config));
            }

            // consoleTable(tableJson);
        })
        .catch(err => {
            if (err !== null)
                console.error(err.message);
        });
} catch(err) {
    if (err) {
        console.error(err.message);
    }
}

/**
 * @table https://www.npmjs.com/package/table#table-usage-text-wrapping
 * configure config
 * @typedef {function}
 * @param {Number} columns number of columns
 * @param {Array} tableProps table column header
 * @return {Object} config object
 */
function tableConfig(columns, tableProps) {
    let config = {
        columns: {}
    };
    for (let i = 0; i < columns; i++) {
        let col = i;
        config.columns[i] = {
            alignment: 'left',
            width: tableProps[i].match(/^[-a-z]+\s?/i)[0].length + 1
        }
    }

    return config;
}

/**
 * @table https://www.npmjs.com/package/table#table-usage-text-wrapping
 * parse data for @table
 * @typedef {function}
 * @param {Number} arrJsonObj JSON object containing the data to be parsed to array
 * @return {Array} dataArr 2D array containing the parsed data
 */
function tableData(arrJsonObj) {
    // console.log(arrJsonObj);
    let dataArr = [];
    for (let i = 0, n = arrJsonObj.length; i < n; i++) {
        dataArr[i] = Object.values(arrJsonObj[i]);
    }
    return dataArr;
}
